import { Module } from 'vuex';
import { Route } from '@/shared/routes/routes.service';

export const routesStore: Module<any, any> = {
  state: {
    route: {
      path: '',
      predicate: '',
      filters: [],
      serviceId: 'AGITELIER-GATEWAY',
      instanceId: 'AGITELIER-GATEWAY',
      instanceUri: '',
      order: 0,
    } as Route,
  },
  getters: {
    route: state => state.route,
  },
  mutations: {
    setRoute(state, route) {
      state.route = route;
    },
  },
};
