/**
 * Spring MVC REST controllers.
 */
package com.gitlab.agitelier.gateway.web.rest;
