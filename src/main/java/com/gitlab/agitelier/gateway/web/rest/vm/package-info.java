/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.agitelier.gateway.web.rest.vm;
